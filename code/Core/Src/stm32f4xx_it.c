/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    stm32f4xx_it.c
  * @brief   Interrupt Service Routines.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f4xx_it.h"
/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN TD */

/* USER CODE END TD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN PV */

int calc_speed(Polo_Ctrl * pololu,  int time_for_movement, int angle_to_reach)
{
	// Dane testowe: 5 sek, 130 st, 0st
	// gear_ratio = 320


	int angle_to_move = angle_to_reach - pololu->rot_ctrl.actual_angle; // 130;
	float time_for_avg_speed = 0.8 * time_for_movement; // 4s;
	float avg_speed = angle_to_move / time_for_avg_speed;  // avg_sec_gear_angle_speed #32.5;
	avg_speed = avg_speed * pololu->gear_ratio; // avg_sec_motor_angle_speed 10400
	avg_speed = avg_speed / 360; // avg_sec_motor_rot_speed 28.8
	avg_speed = avg_speed * 60;

	pololu->avg_speed = (int) avg_speed;

	return (int) avg_speed/32;
	// here we have speed to reach in end phase of acceleration
	/*
	time_for_acc = time_for_movement * 0.2; // needed for formula for limit for 10ms 1s;

	 * if 1 s, then we need to reach max 1/100 of avg speed
	 * 10ms/1s

	acc_steps_count = time_for_acc/50ms; # 20;

	acc_step = avg_speed / acc_steps_count; #1.625 degree/50 ms - increment of speed (not motor);
	//for motor value per minute is neeeded
	acc_motor_step = acc_step * gear_ratio / 60;
	// 8.(6) , but we need to send integer. what about that?
	*/
}

void calc_acc(Polo_Ctrl * pololu, int time_for_movement)
{
	float time_for_acc = time_for_movement * 0.2 * 1000; // from s to ms
	float pololu_update_period = 10;
	int avg_speed = pololu->avg_speed;
	if (avg_speed <0)
	  {
		  avg_speed = avg_speed * -1;
	  }

	float acc_limit = avg_speed * (pololu_update_period/time_for_acc); // 1700 in 1 sec = 170 in 100 ms = 17 in 10ms
	pololu->acc_limit = (int) acc_limit;
}

void set_speed(Polo_Ctrl pololu)
{
	  uint8_t set_speed[5];
	  uint16_t speed_size = 5;
	  uint32_t speed_timeout = 100;
	  int motor_forward = 0x05;
	  int motor_backwards = 0x06;
	  int command = motor_forward;
	  if (pololu.avg_speed <0)
	  {
		  command = motor_backwards;
		  pololu.avg_speed = pololu.avg_speed * -1;
	  }

	  set_speed[0]= 0xAA;              // Pololu protocol identifier
	  set_speed[1] = pololu.device_no; // Pololu device number
	  set_speed[2]=command;               // Set speed command (0x85) with most significant bit cleared (as pololu protocol requires).
	  set_speed[3]=pololu.avg_speed & 0x1F;
	  set_speed[4]=pololu.avg_speed >> 5;
	  //set_speed[3]=0;
	  //set_speed[4]=0x15;

	  HAL_UART_Transmit(pololu.huart, set_speed, speed_size, speed_timeout);
};

void set_acc(Polo_Ctrl pololu)
{
	  uint8_t set_speed[6];
	  uint16_t speed_size = 6;
	  uint32_t speed_timeout = 100;
	  int acceleration = 1;
	  int deceleration = 2;


	  set_speed[0]= 0xAA;              // Pololu protocol identifier
	  set_speed[1] = pololu.device_no; // Pololu device number
	  set_speed[2]=0x22;               // Set motor limit command (0x85) with most significant bit cleared (as pololu protocol requires).
	  set_speed[3]= acceleration;
	  set_speed[4]=pololu.acc_limit & 0x7F;
	  set_speed[5]=pololu.acc_limit >>7;

	  HAL_UART_Transmit(pololu.huart, set_speed, speed_size, speed_timeout);

	  set_speed[3]=deceleration;
	  HAL_UART_Transmit(pololu.huart, set_speed, speed_size, speed_timeout);
};

void reset_pololu(Polo_Ctrl pololu)
{
	  uint8_t reset[1];
	  uint16_t reset_size = 1;
	  uint32_t reset_timeout = 100;

	  reset[0]=0x83;
	  HAL_UART_Transmit(pololu.huart, reset, reset_size, reset_timeout);
};
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/
extern TIM_HandleTypeDef htim3;
extern UART_HandleTypeDef huart5;
/* USER CODE BEGIN EV */

/* USER CODE END EV */

/******************************************************************************/
/*           Cortex-M4 Processor Interruption and Exception Handlers          */
/******************************************************************************/
/**
  * @brief This function handles Non maskable interrupt.
  */
void NMI_Handler(void)
{
  /* USER CODE BEGIN NonMaskableInt_IRQn 0 */

  /* USER CODE END NonMaskableInt_IRQn 0 */
  /* USER CODE BEGIN NonMaskableInt_IRQn 1 */
  while (1)
  {
  }
  /* USER CODE END NonMaskableInt_IRQn 1 */
}

/**
  * @brief This function handles Hard fault interrupt.
  */
void HardFault_Handler(void)
{
  /* USER CODE BEGIN HardFault_IRQn 0 */

  /* USER CODE END HardFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_HardFault_IRQn 0 */
    /* USER CODE END W1_HardFault_IRQn 0 */
  }
}

/**
  * @brief This function handles Memory management fault.
  */
void MemManage_Handler(void)
{
  /* USER CODE BEGIN MemoryManagement_IRQn 0 */

  /* USER CODE END MemoryManagement_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_MemoryManagement_IRQn 0 */
    /* USER CODE END W1_MemoryManagement_IRQn 0 */
  }
}

/**
  * @brief This function handles Pre-fetch fault, memory access fault.
  */
void BusFault_Handler(void)
{
  /* USER CODE BEGIN BusFault_IRQn 0 */

  /* USER CODE END BusFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_BusFault_IRQn 0 */
    /* USER CODE END W1_BusFault_IRQn 0 */
  }
}

/**
  * @brief This function handles Undefined instruction or illegal state.
  */
void UsageFault_Handler(void)
{
  /* USER CODE BEGIN UsageFault_IRQn 0 */

  /* USER CODE END UsageFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_UsageFault_IRQn 0 */
    /* USER CODE END W1_UsageFault_IRQn 0 */
  }
}

/**
  * @brief This function handles System service call via SWI instruction.
  */
void SVC_Handler(void)
{
  /* USER CODE BEGIN SVCall_IRQn 0 */

  /* USER CODE END SVCall_IRQn 0 */
  /* USER CODE BEGIN SVCall_IRQn 1 */

  /* USER CODE END SVCall_IRQn 1 */
}

/**
  * @brief This function handles Debug monitor.
  */
void DebugMon_Handler(void)
{
  /* USER CODE BEGIN DebugMonitor_IRQn 0 */

  /* USER CODE END DebugMonitor_IRQn 0 */
  /* USER CODE BEGIN DebugMonitor_IRQn 1 */

  /* USER CODE END DebugMonitor_IRQn 1 */
}

/**
  * @brief This function handles Pendable request for system service.
  */
void PendSV_Handler(void)
{
  /* USER CODE BEGIN PendSV_IRQn 0 */

  /* USER CODE END PendSV_IRQn 0 */
  /* USER CODE BEGIN PendSV_IRQn 1 */

  /* USER CODE END PendSV_IRQn 1 */
}

/**
  * @brief This function handles System tick timer.
  */
void SysTick_Handler(void)
{
  /* USER CODE BEGIN SysTick_IRQn 0 */

  /* USER CODE END SysTick_IRQn 0 */
  HAL_IncTick();
  /* USER CODE BEGIN SysTick_IRQn 1 */

  /* USER CODE END SysTick_IRQn 1 */
}

/******************************************************************************/
/* STM32F4xx Peripheral Interrupt Handlers                                    */
/* Add here the Interrupt Handlers for the used peripherals.                  */
/* For the available peripheral interrupt handler names,                      */
/* please refer to the startup file (startup_stm32f4xx.s).                    */
/******************************************************************************/

/**
  * @brief This function handles EXTI line0 interrupt.
  */
void EXTI0_IRQHandler(void)
{
  /* USER CODE BEGIN EXTI0_IRQn 0 */
  kill_switch = 1;
  /* USER CODE END EXTI0_IRQn 0 */
  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_0);
  /* USER CODE BEGIN EXTI0_IRQn 1 */

  /* USER CODE END EXTI0_IRQn 1 */
}

/**
  * @brief This function handles TIM3 global interrupt.
  */
void TIM3_IRQHandler(void)
{
  /* USER CODE BEGIN TIM3_IRQn 0 */
	if (cnt % 2 == 0)
	{
			for (int j=0; j<1; j++)
			{
			calc_speed(&pololus[j], rehab_procedure[cnt], rehab_procedure[cnt+j+1]);
			calc_acc(&pololus[j], rehab_procedure[cnt]);
			reset_pololu(pololus[j]);
			set_acc(pololus[j]);
			set_speed(pololus[j]);
			pololus[j].rot_ctrl.actual_angle=rehab_procedure[cnt+j+1];
			kill_switch = 1;
			}


	TIM3->ARR= (int) rehab_procedure[cnt] * 800;
	}
	else
	{
		for (int j=0; j<1; j++)
		{
		pololus[j].avg_speed = 0;
		reset_pololu(pololus[j]);
		set_speed(pololus[j]);
		kill_switch = 1;
		}
		TIM3->ARR= (int) rehab_procedure[cnt-1] * 200;
	}

	cnt++;
	if (cnt==6)
		cnt=0;

  /* USER CODE END TIM3_IRQn 0 */
  HAL_TIM_IRQHandler(&htim3);
  /* USER CODE BEGIN TIM3_IRQn 1 */

  /* USER CODE END TIM3_IRQn 1 */
}

/**
  * @brief This function handles UART5 global interrupt.
  */
void UART5_IRQHandler(void)
{
  /* USER CODE BEGIN UART5_IRQn 0 */

  /* USER CODE END UART5_IRQn 0 */
  HAL_UART_IRQHandler(&huart5);
  /* USER CODE BEGIN UART5_IRQn 1 */

  /* USER CODE END UART5_IRQn 1 */
}

/* USER CODE BEGIN 1 */

/* USER CODE END 1 */

